# Installers

These scripts install useful packages. Some are distro-sepcific, in which case, the distro is a prefix of the scripts name. Scripts that are (theoretically) universal have an `all-` prefix.
