#!/bin/sh
#
# Installs various fonts on a fresh arch installation

install_aarch64() {
	pacman --noconfirm -S \
		adobe-source-han-sans-jp-fonts \
		adobe-source-han-serif-jp-fonts \
		otf-ipafont \
		ttf-hanazono \
		ttf-sazanami \
		adobe-source-han-sans-otc-fonts \
		adobe-source-han-serif-otc-fonts \
		noto-fonts-cjk \
		adobe-source-han-sans-otc-fonts \
		ttf-khmer \
		ttf-tibetan-machine \
		noto-fonts-emoji \
		ttf-joypixels \
		gnu-free-fonts \
		ttf-arphic-uming \
		ttf-indic-otf \
		texlive-core \
		texlive-fontsextra \
		otf-latin-modern \
		otf-latinmodern-math \
		ttf-junicode \
		gentium-plus-font \
		ttf-linux-libertine \
		cantarell-fonts \
		inter-font \
		ttf-opensans \
		adobe-source-sans-pro-fonts \
		adobe-source-code-pro-fonts \
		ttf-anonymous-pro \
		ttf-cascadia-code \
		ttf-fantasque-sans-mono \
		otf-fantasque-sans-mono \
		ttf-fira-mono \
		otf-fira-mono \
		ttf-fira-code \
		ttf-jetbrains-mono \
		ttf-monofur \
		ttf-bitstream-vera \
		ttf-ibm-plex \
		ttf-croscore \
		ttf-dejavu \
		ttf-droid \
		ttf-liberation \
		ttf-linux-libertine \
		noto-fonts \
		ttf-roboto \
		font-bh-ttf \
		tex-gyre-fonts \
		ttf-ubuntu-font-family \
		dina-font \
		tamsyn-font \
		terminus-font \
		bdf-unifont
}

install_amd64() {
	pacman --noconfirm -S \
		adobe-source-han-sans-jp-fonts \
		adobe-source-han-serif-jp-fonts \
		otf-ipafont \
		ttf-hanazono \
		ttf-sazanami \
		adobe-source-han-sans-otc-fonts \
		adobe-source-han-serif-otc-fonts \
		noto-fonts-cjk \
		adobe-source-han-sans-otc-fonts \
		ttf-khmer \
		ttf-tibetan-machine \
		noto-fonts-emoji \
		ttf-joypixels \
		gnu-free-fonts \
		ttf-arphic-uming \
		ttf-indic-otf \
		texlive-core \
		texlive-fontsextra \
		otf-latin-modern \
		otf-latinmodern-math \
		ttf-junicode \
		gentium-plus-font \
		ttf-linux-libertine \
		cantarell-fonts \
		inter-font \
		ttf-opensans \
		adobe-source-sans-pro-fonts \
		adobe-source-code-pro-fonts \
		ttf-anonymous-pro \
		ttf-cascadia-code \
		ttf-fantasque-sans-mono \
		otf-fantasque-sans-mono \
		ttf-fira-mono \
		otf-fira-mono \
		ttf-fira-code \
		ttf-jetbrains-mono \
		ttf-monofur \
		ttf-bitstream-vera \
		ttf-ibm-plex \
		ttf-croscore \
		ttf-dejavu \
		ttf-droid \
		ttf-liberation \
		ttf-linux-libertine \
		noto-fonts \
		ttf-roboto \
		font-bh-ttf \
		tex-gyre-fonts \
		ttf-ubuntu-font-family \
		terminus-font-otb \
		dina-font \
		tamsyn-font \
		terminus-font \
		bdf-unifont
}

if [ $(id -u) -ne 0 ]; then
	printf "Please run the script again with sudo.\n"
	exit -1
fi

pacman -Syu --noconfirm
case $(uname -a) in
	*x86_64*) install_amd64 ;;
	*aarch64*) install_aarch64 ;;
esac
