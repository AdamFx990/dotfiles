#!/bin/sh

if [ $(id -u) -ne 0 ]; then
	printf "Please run the script again with sudo.\n"
	exit -1
fi

pacman -Syu --noconfirm
pacman --noconfirm -S \
    glu \
    mesa \
    sdl sdl_gfx sdl_image sdl_mixer sdl_net sdl_sound sdl_ttf \
    sdl2 sdl2_gfx sdl2_image sdl2_mixer sdl2_net sdl2_ttf lua-sdl2 \
    sdl2_net \
    zlib
