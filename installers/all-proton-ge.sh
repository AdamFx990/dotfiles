#!/bin/sh

TARGET_DIR=$HOME/.local/share/Steam/compatibilitytools.d/
mkdir -p $TARGET_DIR

API_ENDPOINT=https://api.github.com/repos/GloriousEggroll/proton-ge-custom

install_release() {
	OUT_PATH=/tmp/proton-ge
	OUT_FILE=${OUT_PATH}/proton-ge.tar.gz
	mkdir -p $OUT_PATH

	printf 'Downloading: %s\n' $1
	curl -L -o $OUT_FILE $1

	printf 'Extracting tarball to %s\n' $TARGET_DIR
	tar xvzf $OUT_FILE -C $TARGET_DIR
	rm $OUT_FILE
}

get_latest_release() {
	PROTON_GE_LATEST=$(curl -s ${API_ENDPOINT}/releases/latest \
		| grep 'browser_' \
		| grep '.tar.gz' \
		| cut -d\" -f4)

	install_release $PROTON_GE_LATEST
	exit 0
}

get_all_releases() {
	PROTON_GE_RELEASES=$(curl -s ${API_ENDPOINT}/releases \
		| grep 'browser_' \
		| grep '.tar.gz' \
		| cut -d\" -f4)

	for RELEASE in ${PROTON_GE_RELEASES}; do
		install_release $RELEASE
	done
	exit 0
}

while [ $# -gt 0 ]; do
	case $1 in
		-a | --all)
			get_all_releases
			shift
		;;
	esac
done

get_latest_release
