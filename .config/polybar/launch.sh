#!/usr/bin/env sh

## Add this to your wm startup file.

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch bar on each monitor
for MON in $(polybar --list-monitors | cut -d ":" -f1); do
    MONITOR=$MON polybar -c ~/.config/polybar/config.ini --reload main &
done
# polybar -c ~/.config/polybar/config.ini main &
# polybar -c ~/.config/polybar/config.ini aux &
